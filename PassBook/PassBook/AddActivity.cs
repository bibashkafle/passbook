﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PassBook.Model;
using PassBook.Service;

namespace PassBook
{
    [Activity(Label = "@string/ApplicationName", Theme = "@android:style/Theme.Material.Light")]
    public class AddActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Add);

            Button btnAdd = FindViewById<Button>(Resource.Id.btnAdd);
            btnAdd.Click += btnAdd_Click;
        }

        protected void btnAdd_Click(object obj, EventArgs arg)
        {
            var system = FindViewById<EditText>(Resource.Id.system);
            var userName = FindViewById<EditText>(Resource.Id.userName);
            var password = FindViewById<EditText>(Resource.Id.password);
            var url = FindViewById<EditText>(Resource.Id.url);
            var remarks = FindViewById<EditText>(Resource.Id.remarks);

            var input = new PassbookDataModel(system.Text,userName.Text,password.Text,url.Text,remarks.Text);
            new PassbookService().Save(input);

            system.Text = "";
            userName.Text = "";
            password.Text = "";
            url.Text = "";
            remarks.Text = "";
            Toast.MakeText(this, "Data Saved successfully", ToastLength.Short).Show();
        }
    }
}