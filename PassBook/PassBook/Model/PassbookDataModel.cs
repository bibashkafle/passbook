﻿namespace PassBook.Model
{
    public class PassbookDataModel
    {
        public int Id { get; set; }
        public string SysInfo { get; set; }
        public string UserName { get; set; }
        public string Pwd { get; set; }
        public string Url { get; set; }
        public string Remarks { get; set; }
        public string CreatedDate { get; set; }

        public PassbookDataModel()
        {

        }
        public PassbookDataModel(string system, string username, string password, string uri, string remark)
        {
            SysInfo = system;
            UserName = username;
            Pwd = password;
            Url = uri;
            Remarks = remark;
        }
    }
}