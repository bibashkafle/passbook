﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;

namespace PassBook
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true, Icon = "@drawable/icon", Theme = "@android:style/Theme.Material.Light.NoActionBar")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);

            FindViewById<EditText>(Resource.Id.authUserId).Text = "admin";
            FindViewById<EditText>(Resource.Id.authUserPwd).Text = "pwd";

            Button btnLogin = FindViewById<Button>(Resource.Id.authBtnLogin);
            btnLogin.Click += authBtnLogin_Click;

           
        }

        protected void authBtnLogin_Click(object obj, EventArgs arg)
        {
            var userName = FindViewById<EditText>(Resource.Id.authUserId).Text;
            var password = FindViewById<EditText>(Resource.Id.authUserPwd).Text;
            if (userName.Equals("admin") && password.Equals("pwd"))
            {
                //Toast.MakeText(this, "Login Success", ToastLength.Short).Show();               
                StartActivity(typeof(LandingActivity));
            }
        }
    }
}

