﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using PassBook.Model;
using PassBook.Service;

namespace PassBook
{
    [Activity(Label = "@string/ApplicationName", Theme = "@android:style/Theme.Material.Light")]
    public class DataListActivity : ListActivity
    {
        IPassbookService _service;
        private List<PassbookDataModel> dataset;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _service = new PassbookService();
            SetContentView(Resource.Layout.DataList);
            dataset = _service.Get();
            ListAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, dataset.Select(x=>x.SysInfo).ToList());
        }

    //Over ride the click method to implement custom logi 
        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {
            var datarow = dataset[position];
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("System: " + datarow.SysInfo);
            sb.AppendLine("User Name: " + datarow.UserName);
            sb.AppendLine("Password: " + datarow.Pwd);
            sb.AppendLine("Created On: " + datarow.CreatedDate);
            sb.AppendLine("\n" + datarow.Remarks);
           AlertDialog.Builder alertDiag = new AlertDialog.Builder(this);
            alertDiag.SetTitle("Detail");
            alertDiag.SetMessage(sb.ToString());
            alertDiag.SetNegativeButton("Cancel", (sent, args)=> { alertDiag.Dispose(); });
            alertDiag.SetPositiveButton("Delete", (sent, args) => {               
                var res = _service.Delete(datarow.Id);
                Toast.MakeText(this, res.Message, ToastLength.Long).Show();
                if (res.IsSuccess)
                {
                    alertDiag.Dispose();
                    StartActivity(typeof(DataListActivity));
                }
            });
            Dialog diag = alertDiag.Create();
            diag.Show();
        }
    }
}