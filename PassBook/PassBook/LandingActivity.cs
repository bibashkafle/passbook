﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace PassBook
{
    [Activity(Label = "@string/ApplicationName", Theme = "@android:style/Theme.Material.Light")]
    public class LandingActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Landing);

            Button btnhowList = FindViewById<Button>(Resource.Id.showList);
            btnhowList.Click += btnhowList_Click;

            Button btnAddPage = FindViewById<Button>(Resource.Id.showAddPage);
            btnAddPage.Click += btnAddPage_Click;
        }
        protected void btnhowList_Click(object obj, EventArgs arg)
        {
            StartActivity(typeof(DataListActivity));
        }

        protected void btnAddPage_Click(object obj, EventArgs arg)
        {
            StartActivity(typeof(AddActivity));
        }
    }
}