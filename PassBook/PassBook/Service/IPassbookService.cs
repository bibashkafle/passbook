﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PassBook.Model;

namespace PassBook.Service
{
    public interface IPassbookService
    {

        ApiResponse Save(PassbookDataModel value);
        ApiResponse Update(PassbookDataModel value);
        ApiResponse Delete(int id);
        List<PassbookDataModel> Get();
        PassbookDataModel Get(int id);
    }
}