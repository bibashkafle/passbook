﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace PassBook.Service
{
    public class RestClient
    {
        public Uri BaseUrl { get; set; }
        private Dictionary<string, string> headers;
        public RestClient()
        {
            headers = new Dictionary<string, string>();
        }

        public RestClient(Uri uri)
        {
            BaseUrl = uri;
            headers = new Dictionary<string, string>();
        }

        private HttpWebRequest webRequest;

        public T Get<T>(string path = "")
        {
            T resultToReturn = Activator.CreateInstance<T>();
            try
            {
                webRequest = (HttpWebRequest)WebRequest.Create(string.IsNullOrWhiteSpace(path) ? BaseUrl : new Uri(BaseUrl.AbsoluteUri + path));
                webRequest.Method = "GET";
                webRequest.ContentType = "application/json";
                webRequest.Accept = "text/json";

                foreach (var element in headers)
                {
                    webRequest.Headers.Add(element.Key, element.Value);
                }

                var httpWebResponse = (HttpWebResponse)webRequest.GetResponse();
                if (httpWebResponse.StatusCode.Equals(HttpStatusCode.OK))
                {
                    var responseString = new StreamReader(httpWebResponse.GetResponseStream()).ReadToEnd();
                    resultToReturn = JsonConvert.DeserializeObject<T>(responseString);
                }
                else
                {
                    throw new Exception("Error: " + httpWebResponse.StatusCode + ",Description: " + httpWebResponse.StatusDescription);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return resultToReturn;
        }
        public T Post<T>(object value, string path = "")
        {
            T resultToReturn = Activator.CreateInstance<T>();
            try
            {
                var url = string.IsNullOrWhiteSpace(path) ? BaseUrl : new Uri(BaseUrl.AbsoluteUri + path);
                webRequest = (HttpWebRequest)WebRequest.Create(string.IsNullOrWhiteSpace(path) ? BaseUrl : new Uri(BaseUrl.AbsoluteUri + path));
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.Accept = "text/json";

                foreach (var element in headers)
                {
                    webRequest.Headers.Add(element.Key, element.Value);
                }

                using (var writter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(value);
                    writter.Write(json);
                    writter.Close();
                }
                var httpWebResponse = (HttpWebResponse)webRequest.GetResponse();
                if (httpWebResponse.StatusCode.Equals(HttpStatusCode.OK))
                {
                    var responseString = new StreamReader(httpWebResponse.GetResponseStream()).ReadToEnd();
                    resultToReturn = JsonConvert.DeserializeObject<T>(responseString);
                }
                else
                {
                    throw new Exception("Error: " + httpWebResponse.StatusCode + ",Description: " + httpWebResponse.StatusDescription);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return resultToReturn;
        }
        public T Put<T>(object value, string path = "")
        {
            T resultToReturn = Activator.CreateInstance<T>();
            try
            {
                var url = string.IsNullOrWhiteSpace(path) ? BaseUrl : new Uri(BaseUrl.AbsoluteUri + path);

                webRequest = (HttpWebRequest)WebRequest.Create(string.IsNullOrWhiteSpace(path) ? BaseUrl : new Uri(BaseUrl.AbsoluteUri + path));

                webRequest.Method = "PUT";
                webRequest.ContentType = "application/json";
                webRequest.Accept = "text/json";

                foreach (var element in headers)
                {
                    webRequest.Headers.Add(element.Key, element.Value);
                }

                using (var writter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(value);
                    writter.Write(json);
                    writter.Close();
                }
                var httpWebResponse = (HttpWebResponse)webRequest.GetResponse();
                if (httpWebResponse.StatusCode.Equals(HttpStatusCode.OK))
                {
                    var responseString = new StreamReader(httpWebResponse.GetResponseStream()).ReadToEnd();
                    resultToReturn = JsonConvert.DeserializeObject<T>(responseString);
                }
                else
                {
                    throw new Exception("Error: " + httpWebResponse.StatusCode + ",Description: " + httpWebResponse.StatusDescription);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return resultToReturn;
        }
        public T Delete<T>(string path = "")
        {
            T resultToReturn = Activator.CreateInstance<T>();
            try
            {
                if (!string.IsNullOrWhiteSpace(path))
                    BaseUrl = new Uri(BaseUrl.AbsoluteUri + path);

                webRequest = (HttpWebRequest)WebRequest.Create(BaseUrl);

                webRequest.Method = "DELETE";
                webRequest.ContentType = "application/json";
                webRequest.Accept = "text/json";

                foreach (var element in headers)
                {
                    webRequest.Headers.Add(element.Key, element.Value);
                }

                var httpWebResponse = (HttpWebResponse)webRequest.GetResponse();
                if (httpWebResponse.StatusCode.Equals(HttpStatusCode.OK))
                {
                    var responseString = new StreamReader(httpWebResponse.GetResponseStream()).ReadToEnd();
                    resultToReturn = JsonConvert.DeserializeObject<T>(responseString);
                }
                else
                {
                    throw new Exception("Error: " + httpWebResponse.StatusCode + ",Description: " + httpWebResponse.StatusDescription);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return resultToReturn;
        }

        public void AddHeader(string key, string value)
        {
            headers.Add(key, value);
        }
        public void AddHeader(List<KeyValuePair<string, string>> value)
        {
            value.ForEach(x =>
            {
                if (!headers.ContainsKey(x.Key))
                    headers.Add(x.Key, x.Value);
                else
                    headers[x.Key] = x.Value;
            });
        }
        public void RemoveHeader(string key)
        {
            headers.Remove(key);
        }
        public void ClearHeader()
        {
            headers.Clear();
        }
    }
}