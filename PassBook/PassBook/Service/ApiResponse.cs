﻿namespace PassBook.Service
{
    public class ApiResponse
    {
        public int Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}