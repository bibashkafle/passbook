﻿using System;
using System.Collections.Generic;
using PassBook.Model;

namespace PassBook.Service
{
    public class PassbookService : IPassbookService
    {
        RestClient restClient;
        
        public PassbookService()
        {
            restClient = new RestClient(new Uri("http://passbook.bibashkafle.com.np"));
        }

        public ApiResponse Delete(int id)
        {
            var dict = new Dictionary<string, int>();
            dict.Add("id", id);
            return restClient.Put<ApiResponse>(dict,$"/api/delete");
        }

        public List<PassbookDataModel> Get()
        {
            return restClient.Get<List<PassbookDataModel>>($"/api/get");
        }

        public PassbookDataModel Get(int id)
        {
            return restClient.Get<PassbookDataModel>($"/api/get/{id}");
        }

        public ApiResponse Save(PassbookDataModel value)
        {
            return restClient.Post<ApiResponse>(value, $"/api/add");
        }

        public ApiResponse Update(PassbookDataModel value)
        {
            return restClient.Put<ApiResponse>(value, $"/api/add");
        }
    }
}